import java.util.List;
import java.util.ArrayList;

public class State {
    private String state;
    private double value;
    private List<State> branchs;

    State(String state, double value) {
        this.state = state;
        this.value = value;
        this.branchs = new ArrayList<>();
    }

    public double getValue() {
        return this.value;
    }

    public String getState() {
        return this.state;
    }

    public List<State> getBranchs() {
        return this.branchs;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public static State init() {
        // first level
        State root = new State("7", 0.5);

        // second level
        root.branchs.add(new State("61", 0.5));
        root.branchs.add(new State("52", 0.5));
        root.branchs.add(new State("43", 0.5));

        // third level
        root.branchs.get(0).branchs.add(new State("511", 0.5));
        root.branchs.get(0).branchs.add(new State("421", 0.5));
        
        root.branchs.get(1).branchs.add(new State("421", 0.5));
        root.branchs.get(1).branchs.add(new State("322", 0.5));
        
        root.branchs.get(2).branchs.add(new State("421", 0.5));
        root.branchs.get(2).branchs.add(new State("331", 0.5));

        // fourth level
        root.branchs.get(0).branchs.get(0).branchs.add(new State("4111", 0.5));
        root.branchs.get(0).branchs.get(0).branchs.add(new State("3211", 0.5));

        root.branchs.get(0).branchs.get(1).branchs.add(new State("3211", 0.5));
        
        root.branchs.get(1).branchs.get(0).branchs.add(new State("3211", 0.5));
        
        root.branchs.get(1).branchs.get(1).branchs.add(new State("2221", 1));
        
        root.branchs.get(2).branchs.get(0).branchs.add(new State("3211", 0.5));
        root.branchs.get(2).branchs.get(1).branchs.add(new State("3211", 0.5));

        //fifth level
        root.branchs.get(0).branchs.get(0).branchs.get(0).branchs.add(new State("31111", 0.5));
        root.branchs.get(0).branchs.get(0).branchs.get(1).branchs.add(new State("22111", 0));
        root.branchs.get(0).branchs.get(1).branchs.get(0).branchs.add(new State("22111", 0));
        root.branchs.get(1).branchs.get(0).branchs.get(0).branchs.add(new State("22111", 0));
        root.branchs.get(2).branchs.get(0).branchs.get(0).branchs.add(new State("22111", 0));
        root.branchs.get(2).branchs.get(1).branchs.get(0).branchs.add(new State("22111", 0));

        // sixth level
        root.branchs.get(0).branchs.get(0).branchs.get(0).branchs.get(0).branchs.add(new State("211111", 1));

        return root;
    }
}