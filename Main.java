import java.util.Random;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // init tree states, learningRate, epochs
        State node = State.init();
        double learningRate = 0.1;
        int epochs = 100;
        boolean computerFirstTurn = true;

        // learning, eGreedy = 0.9
        for (int i = 0; i < epochs; i++) {
            reinforcement(0.9, learningRate, node, false, computerFirstTurn, false);
        }
        // testing, eGreedy = 0.1
        reinforcement(0.1, learningRate, node, true, computerFirstTurn, true);
    }

    public static void reinforcement(double eGreedy, double learningRate, State node, boolean showResult,
            boolean computerFirstTurn, boolean inputControlledEnemy) {
        State iterator = node;
        boolean computerWins = false;
        boolean computerTurn = computerFirstTurn;
        Scanner scanner = new Scanner(System.in);

        List<State> routes = new ArrayList<>(); // menyimpan jejak rute
        routes.add(iterator);

        while (iterator.getBranchs().size() != 0) {
            int nextNodeIndex = -1;

            if (!computerTurn && inputControlledEnemy) {
                System.out.println("CURRENT STATE: " + iterator.getState());
                System.out.println("STATE TO CHOOSE: ");

                for (int i = 0; i < iterator.getBranchs().size(); i++) {
                    System.out.println("[" + i + "] " + iterator.getBranchs().get(i).getState());
                }

                System.out.print("YOUR CHOICE? ");
                nextNodeIndex = scanner.nextInt();
                System.out.print("");
            } else {
                double choiceDeterminator = new Random().nextDouble();

                if (choiceDeterminator < eGreedy) {
                    if (iterator.getBranchs().size() == 1) {
                        nextNodeIndex = 0;
                    } else {
                        nextNodeIndex = getRandomNumber(iterator.getBranchs().size() - 1, 0);
                    }
                } else {
                    if (iterator.getBranchs().size() == 1) {
                        nextNodeIndex = 0;
                    } else {
                        int selectedIndex = -1; // kalo first turn maka cari value ter-max, kalo enda maka cari value
                                                // ter-min

                        for (int i = 0; i < iterator.getBranchs().size(); i++) {
                            if (i == 0) {
                                selectedIndex = i;
                            } else {
                                if (computerFirstTurn) {
                                    if (iterator.getBranchs().get(i).getValue() > iterator.getBranchs()
                                            .get(selectedIndex)
                                            .getValue()) {
                                        selectedIndex = i;
                                    }
                                } else {
                                    if (iterator.getBranchs().get(i).getValue() < iterator.getBranchs()
                                            .get(selectedIndex)
                                            .getValue()) {
                                        selectedIndex = i;
                                    }
                                }
                            }
                        }

                        List<Integer> selectedIndexValueCount = new ArrayList<>();
                        for (int i = 0; i < iterator.getBranchs().size(); i++) {
                            if (iterator.getBranchs().get(i).getValue() == iterator.getBranchs().get(selectedIndex)
                                    .getValue()) {
                                selectedIndexValueCount.add(i);
                            }
                        }

                        if (selectedIndexValueCount.size() == 1) {
                            nextNodeIndex = selectedIndexValueCount.get(0);
                        } else {
                            nextNodeIndex = selectedIndexValueCount
                                    .get(getRandomNumber(selectedIndexValueCount.size() - 1, 0));
                        }
                    }
                }
            }

            State nextNode = iterator.getBranchs().get(nextNodeIndex);
            iterator.setValue(iterator.getValue() + learningRate * (nextNode.getValue() - iterator.getValue()));
            iterator = nextNode;
            routes.add(iterator);

            computerTurn = !computerTurn; // ganti giliran main
        }

        if (computerFirstTurn) {
            if (iterator.getValue() == 1) {
                computerWins = true;
            }
        } else {
            if (iterator.getValue() == 0) {
                computerWins = true;
            }
        }

        if (showResult) {
            boolean computerTurnPrint = computerFirstTurn;
            String enemy = (inputControlledEnemy) ? "USER" : "ENEMY";

            System.out.println("ROUTES:");
            System.out.println("NO\tTURN\tSTATE\tV");
            for (int i = 0; i < routes.size(); i++) {
                if (i == 0) {
                    System.out.println((i + 1) + "\tSTART\t" + routes.get(i).getState() + "\t"
                            + String.format("%,.6f", routes.get(i).getValue()));
                } else {
                    System.out.println((i + 1) + "\t" + ((computerTurnPrint) ? "COMP" : enemy) + "\t"
                            + routes.get(i).getState() + "\t" + String.format("%,.6f", routes.get(i).getValue()));
                    computerTurnPrint = !computerTurnPrint;
                }
            }

            System.out.println("\nRESULT:");
            System.out.println("COMPUTER " + ((computerWins) ? "WIN" : "LOSE"));
        }
    }

    public static int getRandomNumber(int max, int min) {
        return new Random().nextInt(max + 1 - min) + min;
    }
}